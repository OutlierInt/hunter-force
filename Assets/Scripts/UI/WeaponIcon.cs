﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponIcon : MonoBehaviour {
	public Outline iconOutline;
	public Image iconImage;
	public Text iconText;

	public Color backgroundNormalColor = Color.black;
	public Color backgroundHighlightColor = Color.black;
	public Color outlineNormalColor = Color.white;
	public Color outlineHighlightColor = Color.red;

	public void highlight() {
		iconOutline.effectColor = outlineHighlightColor;
	}

	public void unhighlight() {
		iconOutline.effectColor = outlineNormalColor;
	}
}
