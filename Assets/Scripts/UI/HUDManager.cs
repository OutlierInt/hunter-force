﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HUDManager : MonoBehaviour {
	private GameObject player;
	private PlayerMovement playerMover;
	private PlayerShooting playerShooting;
	private PlayerBomber playerBomber;

	//Weapon Bar
	public RectTransform weaponIconHolder;
	public List<WeaponIcon> weaponIcons;
	public WeaponIcon weaponIconPrefab;

	//Speed Bar
	public RectTransform speedBarHolder;
	public Slider speedBar;
	public Text speedBarText;
	private Coroutine speedBarAnimation;
	public float speedBarVisibleTime = 1f;
	public float speedBarHideTime = 0.5f;

	//Score
	public Text scoreText;
	
	//Charge Text
	public Text chargeText;

	void Start() {
		player = FindObjectOfType<PlayerMovement>().gameObject;
		playerMover = player.GetComponent<PlayerMovement>();
		playerShooting = player.GetComponent<PlayerShooting>();
		playerBomber = player.GetComponent<PlayerBomber>();

		setupWeaponIconBar();
	}

	void OnEnable() {
		PlayerMovement.onSpeedChange += animateSpeedBar;
		PlayerShooting.onWeaponSwitch += HighlightWeaponIcons;
		PlayerShooting.onWeaponsUpdated += FullyUpdateWeaponIconBar;
		ScoringManager.onScoreChanged += UpdateScoreText;
		PlayerBomber.onChargingBomb += UpdateChargeText;
		PlayerBomber.onBombReady += BombReadyText;
		PlayerBomber.onBombFire += FireBombText;
		PlayerBomber.onBombDisabled += HideBombText;
	}

	void OnDisable() {
		PlayerMovement.onSpeedChange -= animateSpeedBar;
		PlayerShooting.onWeaponSwitch -= HighlightWeaponIcons;
		PlayerShooting.onWeaponsUpdated -= FullyUpdateWeaponIconBar;
		ScoringManager.onScoreChanged -= UpdateScoreText;
		PlayerBomber.onChargingBomb -= UpdateChargeText;
		PlayerBomber.onBombReady -= BombReadyText;
		PlayerBomber.onBombFire -= FireBombText;
		PlayerBomber.onBombDisabled -= HideBombText;
	}

	#region Weapon Bar Methods
	public void setupWeaponIconBar() {
		//Clear Weapon Bar
		while (weaponIcons.Count > 0) {
			Destroy(weaponIcons[0].gameObject);
			weaponIcons.RemoveAt(0);
		}

		//Make new buttons
		foreach (PlayerWeapon weapon in playerShooting.availableWeapons) {
			WeaponIcon weaponIcon = Instantiate(weaponIconPrefab, weaponIconHolder) as WeaponIcon;
			RectTransform weaponIconTransform = weaponIcon.GetComponent<RectTransform>();
			weaponIconTransform.localScale = Vector3.one;
			weaponIconTransform.anchoredPosition3D = weaponIconTransform.anchoredPosition;

			if(weapon != PlayerWeapon.None) {
				weaponIcon.iconText.text = System.Enum.GetName(typeof(PlayerWeapon), weapon);
			}
			else {
				weaponIcon.iconText.enabled = false;
				weaponIcon.iconOutline.enabled = false;
				weaponIcon.iconImage.enabled = false;
			}
			weaponIcons.Add(weaponIcon);
		}
	}

	public void HighlightWeaponIcons() {
		for(int i = 0; i < playerShooting.availableWeapons.Count; i++) {
			if(i == playerShooting.curWeaponIndex)
				weaponIcons[i].highlight();
			else
				weaponIcons[i].unhighlight();
		}
	}

	public void FullyUpdateWeaponIconBar() {
		setupWeaponIconBar();
		HighlightWeaponIcons();
	}
	#endregion

	#region Speed Bar Methods
	public void animateSpeedBar() {
		UpdateSpeedBar();

		if (speedBarAnimation != null)
			StopCoroutine(speedBarAnimation);

		speedBarAnimation = StartCoroutine(showSpeedBar(speedBarVisibleTime, speedBarHideTime));
	}

	public IEnumerator showSpeedBar(float duration, float hideTime) {
		Vector2 anchoredPosition = speedBarHolder.anchoredPosition;
		speedBarHolder.anchoredPosition = new Vector2(anchoredPosition.x, -5f);

		yield return new WaitForSecondsRealtime(duration);
		
		float slideUpTimer = 0f;
		while(slideUpTimer < hideTime) {
			slideUpTimer += Time.unscaledDeltaTime;
			float slidePosition = Mathf.Lerp(-5f, speedBarHolder.sizeDelta.y, slideUpTimer/hideTime);
			speedBarHolder.anchoredPosition = new Vector2(anchoredPosition.x, slidePosition);
			yield return new WaitForEndOfFrame();
		}
	}

	public void UpdateSpeedBar() {
		float speedPercentage = playerMover.getSpeedPercentage();
		float visualPercentage = Mathf.Lerp(0.25f, 1f, speedPercentage);
		speedBar.value = visualPercentage;
		speedBarText.text = string.Format("Speed {0}%", Mathf.RoundToInt(visualPercentage * 100f));
	}
	#endregion

	public void UpdateScoreText() {
		scoreText.text = (ScoringManager.score).ToString();
	}

	public void UpdateChargeText() {
		float chargePercentage = Mathf.Clamp01(playerBomber.bombCharge / playerBomber.bombChargeTime);
		chargeText.text = string.Format( "{0}%", (int)(chargePercentage * 100f) );
	}

	public void BombReadyText() {
		chargeText.text = "BOMB READY!";
	}

	public void FireBombText() {
		chargeText.text = "FIRING...";
	}

	public void HideBombText() {
		chargeText.text = string.Empty;
	}
}
