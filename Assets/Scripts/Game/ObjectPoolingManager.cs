﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolingManager : MonoBehaviour {
	public static Dictionary<string, ObjectPool> objectPools = new Dictionary<string, ObjectPool>();

	public static void CreateNewObjectPool(string poolName, GameObject objectToCreate, int count) {
		//Create the pool
		ObjectPool pool = new ObjectPool();

		//Instantiate objects
		for(int i = 0; i < count; i++) {
			GameObject go = Instantiate(objectToCreate) as GameObject;
			pool.Push(go);
		}

		//Put pool in dictionary
		objectPools.Add(poolName, pool);
	}

	public static void ClearAllPools() {
		foreach(KeyValuePair<string, ObjectPool> item in objectPools) {
			ObjectPool pool = item.Value;
			pool.Clear();
		}
		objectPools.Clear();
	}

	public static GameObject GetObjectFromPool(string poolName) {
		return GetObjectFromPool(poolName, Vector3.zero, Quaternion.identity, null, Space.World);
	}

	public static GameObject GetObjectFromPool(string poolName, Vector3 position, Quaternion rotation, Transform parent) {
		return GetObjectFromPool(poolName, position, rotation, parent, Space.World, Space.Self);
	}

	public static GameObject GetObjectFromPool(string poolName, Vector3 position, Quaternion rotation, Transform parent, Space positionRotationSpace) {
		return GetObjectFromPool(poolName, position, rotation,parent, positionRotationSpace, positionRotationSpace);
	}

	public static GameObject GetObjectFromPool(string poolName, Vector3 position, Quaternion rotation, Transform parent, Space positionSpace, Space rotationSpace) {
		if(objectPools.ContainsKey(poolName)) {
			GameObject obj = objectPools[poolName].Pop();

			//Empty pool?
			if (obj == null)
				return null;

			//Set parent
			obj.transform.parent = parent;

			//Set Position
			if(positionSpace == Space.World)
				obj.transform.position = position;
			else
				obj.transform.localPosition = position;

			//Set Rotation
			if(rotationSpace == Space.World)
				obj.transform.rotation = rotation;
			else
				obj.transform.localRotation = rotation;

			return obj;
		}

		//No such pool found.
		return null;
	}
}

public class ObjectPool {
	private List<GameObject> objectPool = new List<GameObject>();

	public void Push(GameObject obj) {
		obj.SetActive(false);
		objectPool.Add(obj);
	}

	public GameObject Pop() {
		for(int i = 0; i < objectPool.Count; i++) {
			if(!objectPool[i].activeInHierarchy) {
				GameObject obj = objectPool[i];
				obj.SetActive(true);
				return obj;
			}
		}
		return null;
	}

	public void Clear() {
		while(objectPool.Count > 0) {
			MonoBehaviour.Destroy(objectPool[0]);
			objectPool.RemoveAt(0);
		}
	}
}

public interface PoolableGameObject {
	void Create(Object objectToCreate, Vector3 position, Quaternion rotation, Transform parent);
	void Destroy();
}