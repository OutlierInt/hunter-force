﻿using UnityEngine;
using System.Collections;

public class PauseController : MonoBehaviour {

	private static float savedTimeScale = 1f;
	public static bool paused = false;

	public static void TogglePause() {
		if(!paused)
			Pause();
		else
			Unpause();
	}

	public static void Pause() {
		paused = true;
		savedTimeScale = Time.timeScale;
		Time.timeScale = 0f;
	}

	public static void Unpause() {
		paused = false;
		Time.timeScale = savedTimeScale;
	}
	
}
