﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager main;
	public GameObject player;
	public Transform gameplayPlane;
	public Vector2 gameplayBoundsHorizontal = new Vector2(-36f, 36f);
	public Vector2 gameplayBoundsVertical = new Vector2(-20f, 20f);

	public CameraController cameraCon;
	private float[] XY = {-1, 1};

	void Awake() {
		main = this;
	}

	void Update() {
		if(Input.GetKeyDown(KeyCode.Escape))
			PauseController.TogglePause();
	}

	void OnDrawGizmosSelected() {
		DrawPlayAreaBox();
	}

	public void DrawPlayAreaBox() {
		Vector3 up = gameplayPlane.up;
		Vector3 right = gameplayPlane.right;
		Vector3 topLeftCorner = gameplayPlane.position + (up * gameplayBoundsVertical.x) + (right * gameplayBoundsHorizontal.x);
		Vector3 topRightCorner = gameplayPlane.position + (up * gameplayBoundsVertical.x) + (right * gameplayBoundsHorizontal.y);
		Vector3 bottomLeftCorner = gameplayPlane.position + (up * gameplayBoundsVertical.y) + (right * gameplayBoundsHorizontal.x);
		Vector3 bottomRightCorner = gameplayPlane.position + (up * gameplayBoundsVertical.y) + (right * gameplayBoundsHorizontal.y);
		
		Gizmos.DrawLine(topLeftCorner, topRightCorner);			//Top
		Gizmos.DrawLine(bottomLeftCorner, bottomRightCorner);	//Bottom
		Gizmos.DrawLine(topLeftCorner, bottomLeftCorner);		//Left
		Gizmos.DrawLine(topRightCorner, bottomRightCorner);		//Right
	}

	public static GameObject PlayerObj() {
		return main.player;
	}

	public static Vector3 PlayerPosition() {
		return main.player.transform.position;
	}
}
