﻿using UnityEngine;
using System.Collections;

public class ScoringManager : MonoBehaviour {

	public delegate void EventAction();
	public static EventAction onScoreChanged;

	public static int score = 0;

	public static void AddPoints (int points, int multiplier = 1) {
		score += points * multiplier;
		onScoreChanged();
	}
	
}
