﻿using UnityEngine;
using System.Collections;

public class TargetMover : MonoBehaviour {

	public Transform gameplayPlane;
	public float moveTime = 1f;
	private float moveTimer = 0;
	public float xMin, xMax, yMin, yMax, zMin, zMax;
	private Vector3 prevPosition, goalPosition;

	void Awake() {
		prevPosition = transform.position;
		goalPosition = gameplayPlane.position + new Vector3(
				Mathf.Lerp(xMin, xMax, Random.value),
				Mathf.Lerp(yMin, yMax, Random.value),
				0f);
	}

	void Update () {
		if(moveTimer >= moveTime) {
			moveTimer = 0;
			prevPosition = goalPosition;
			goalPosition = gameplayPlane.position + new Vector3(
				Mathf.Lerp(xMin, xMax, Random.value),
				Mathf.Lerp(yMin, yMax, Random.value),
				Mathf.Lerp(zMin, zMax, Random.value));
		}
		else {
			moveTimer += Time.deltaTime;
			transform.position = Vector3.Lerp(prevPosition, goalPosition, moveTimer/moveTime);
		}
	}
}
