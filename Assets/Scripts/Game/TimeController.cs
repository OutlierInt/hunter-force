﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class TimeController : MonoBehaviour {

	public AudioMixer masterMix;
	public float minTime, maxTime, increment;

	void Update() {
		if(Input.GetKeyDown(KeyCode.Minus)) {
			Time.timeScale = Mathf.Max( minTime, Time.timeScale - increment );
		}
		else if(Input.GetKeyDown(KeyCode.Equals)) {
			Time.timeScale = Mathf.Min( maxTime, Time.timeScale + increment );
		}
		else if(Input.GetKeyDown(KeyCode.Backspace)) {
			Time.timeScale = 1f;
		}

		masterMix.SetFloat("Master Pitch", Time.timeScale);
	}

}
