﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	private Transform gameplayPlane;
	private Transform player;

	public float cameraMoveSpeed = 5f;
	public float cameraDistance = 50f;
	public Vector2 cameraVerticalBounds = Vector2.zero;
	public Vector3 cameraGoalPosition;

	void Start() {
		player = GameManager.main.player.transform;
		gameplayPlane = GameManager.main.gameplayPlane;
	}

	void LateUpdate() {
		if(player == null) return;

		//Align to face the gameplay plane and its center
		transform.localRotation = Quaternion.identity; //gameplayPlane.rotation;
		Vector3 centerPosition = -Vector3.forward * cameraDistance;//gameplayPlane.position - gameplayPlane.forward * cameraDistance;

		//Adjust y-position relative to player's y-position. Try to keep player in the center of screen.
		Vector3 playerOffset = player.position - gameplayPlane.position;
		playerOffset = gameplayPlane.InverseTransformVector(playerOffset);

		//Bound Camera to a certain height
		float boundedVertical = Mathf.Clamp(playerOffset.y, cameraVerticalBounds.x, cameraVerticalBounds.y);

		//Move Camera vertically
		cameraGoalPosition = centerPosition + Vector3.up * boundedVertical;
		transform.localPosition = Vector3.Lerp(transform.localPosition, cameraGoalPosition, Time.deltaTime * cameraMoveSpeed);

	}
}
