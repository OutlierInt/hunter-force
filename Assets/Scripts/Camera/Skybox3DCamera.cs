﻿using UnityEngine;

//Original stolen from a comment here: https://www.youtube.com/watch?v=7uVgI4QkLPk
public class Skybox3DCamera: MonoBehaviour
{ 
	//Improved 3D Skybox Script
	public Camera playerCamera; 
	public float scaleDenominator = 3000f;
	public Transform SkyboxLocation;
	private Camera selfCamera;

	void Awake() {
		selfCamera = GetComponent<Camera>();
	}

	void LateUpdate () {
		if(playerCamera != null) {
			selfCamera.fieldOfView = playerCamera.fieldOfView;
			selfCamera.rect = playerCamera.rect;
			gameObject.transform.rotation = playerCamera.gameObject.transform.rotation;
			gameObject.transform.position = SkyboxLocation.position + playerCamera.transform.position * (1f / scaleDenominator);
		}
	}

}
