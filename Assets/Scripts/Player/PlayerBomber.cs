﻿using UnityEngine;
using System.Collections;

public class PlayerBomber : MonoBehaviour {

	public delegate void EventAction();
	public static EventAction onChargingBomb;
	public static EventAction onBombReady;
	public static EventAction onBombFire;
	public static EventAction onBombDisabled;

	public float bombChargeTime = 15f;
	public float bombCharge = 0;

	public float chargeUpTime = 0.75f;
	public float coolDownTime = 2f;

	public float forwardOffset = 10f;
	public bool firingSuperBomb = false;
	public GameObject chargeEffect;
	private bool canFireBomb = false;

	void Update() {
		if(!firingSuperBomb) {
			if(bombCharge <= bombChargeTime) {
				canFireBomb = false;
				bombCharge += Time.deltaTime;
				if(onChargingBomb != null)
					onChargingBomb();
			}
			else {
				if(!canFireBomb) {
					canFireBomb = true;
					if(onBombReady != null)
						onBombReady();
				}
			}
		}

		if(cInput.GetKeyDown("Bomb") && !firingSuperBomb) {
			if(bombCharge >= bombChargeTime) {
				StartCoroutine(FireSuperBomb());
				bombCharge = 0;
			}
		}
	}

	void OnDisable() {
		StopAllCoroutines();
		bombCharge = 0;
		firingSuperBomb = false;
		chargeEffect.SetActive(false);
		if(onBombDisabled != null)
			onBombDisabled();
	}

	public IEnumerator FireSuperBomb() {
		firingSuperBomb = true;
		canFireBomb = false;

		if (onBombFire != null)
			onBombFire();

		chargeEffect.SetActive(true);
		yield return new WaitForSeconds(chargeUpTime);

		ObjectPoolingManager.GetObjectFromPool("Bomber", transform.position + (transform.right * forwardOffset), Quaternion.identity, transform.parent);

		yield return new WaitForSeconds(coolDownTime);
		chargeEffect.SetActive(false);
		firingSuperBomb = false;
	}

}
