﻿using UnityEngine;
using System.Collections;

public class PlayerModelController : MonoBehaviour {

	private PlayerMovement playerMover;

	[System.NonSerialized]
	public Quaternion initLocalRotation = Quaternion.identity;
	public float rollMax = 85f;
	public float rollSpeed = 3f;
	private float shipRoll = 0;

	public AudioSource speedChangeSound;
	public ParticleSystem[] thrusters;
	public float thrusterAnimationDuration = 1f;
	public float thrusterAnimationSize = 10f;
	public float thrusterAnimationColorHoldTime = 1f;
	public float thrusterAnimationColorFadeDuration = 1f;
	public Color thrusterAnimationColor = Color.yellow;
	private float startSize;
	private Color startColor;
	private Coroutine colorizer;

	void Awake() {
		playerMover = GetComponentInParent<PlayerMovement>();
		if(thrusters != null && thrusters.Length > 0) {
			startSize = thrusters[0].startSize;
			startColor = thrusters[0].startColor;
		}
	}

	void OnEnable() {
		PlayerMovement.onSpeedChange += ChangeThrusterColor;
	}

	void OnDisable() {
		PlayerMovement.onSpeedChange -= ChangeThrusterColor;
	}

	void Update () {
		//Roll on X axis during vertical movement
		shipRoll = Mathf.Lerp(shipRoll, playerMover.inputVector.y, Time.deltaTime * rollSpeed);
		transform.localRotation = initLocalRotation * Quaternion.AngleAxis(rollMax * shipRoll, Vector3.right);

		//Speed Change Animation
		if(cInput.GetKeyDown("SpeedChange"))
			speedChangeAnimation();

		foreach(ParticleSystem thruster in thrusters) {
			//thruster.startColor;
		}
	}

	public void speedChangeAnimation() {
		speedChangeSound.Stop();
		speedChangeSound.Play();
		StartCoroutine(thrusterBoost(thrusterAnimationDuration, thrusterAnimationSize));
	}

	public void ChangeThrusterColor() {
		if(colorizer != null)
			StopCoroutine(colorizer);
		colorizer = StartCoroutine(thrusterColorize(thrusterAnimationColorHoldTime, thrusterAnimationColorFadeDuration));
	}

	private IEnumerator thrusterColorize(float holdTime, float duration) {
		foreach(ParticleSystem thruster in thrusters) {
			thruster.startColor = thrusterAnimationColor;
		}

		yield return new WaitForSecondsRealtime(holdTime);

		float timer = duration;
		while(duration > 0f) {
			foreach(ParticleSystem thruster in thrusters) {
				thruster.startColor = Color.Lerp(startColor, thrusterAnimationColor, timer/duration);
			}
			yield return new WaitForEndOfFrame();
			timer -= Time.unscaledDeltaTime;
		}
	}

	private IEnumerator thrusterBoost(float duration, float thrusterSize) {
		float timer = duration;

		while(duration > 0f) {
			foreach(ParticleSystem thruster in thrusters) {
				thruster.startSize = Mathf.Lerp(startSize, thrusterSize, timer/duration);
			}
			yield return new WaitForEndOfFrame();
			timer -= Time.unscaledDeltaTime;
		}
	}
}
