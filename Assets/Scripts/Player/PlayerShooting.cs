﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public enum PlayerWeapon { Twinshot, Backshot, Snake, Freeway, Hunter, Blade, Railgun, Saber, Bomber, None }
public enum PlayerExtraWeapon { SnakeClone, SnakePulse, MiniBlade, BFGPulse }

public class PlayerShooting : MonoBehaviour {
	public const string PLAYERWEAPONTAG = "Bullet";

	public delegate void EventAction();
	public static EventAction onWeaponSwitch;
	public static EventAction onWeaponsUpdated;

	private PlayerMovement playerMover;
	private PlayerModelController modelCon;
	private PlayerClawController clawCon;

	public bool shootingEnabled = true;

	public List<PlayerWeapon> availableWeapons = new List<PlayerWeapon>(){ PlayerWeapon.Twinshot, PlayerWeapon.Backshot, PlayerWeapon.Snake, PlayerWeapon.Freeway, PlayerWeapon.Hunter };
	private List<PlayerWeapon> differenceTracking;
	public int curWeaponIndex = 0;

	public AudioSource fireSound;
	public float fireRateTime = 0.1f;
	private float fireTimer = 0f;

	public bool angledTwinShots = true;
	private float twinShotAngle = 15f;

	public bool snakeModified = false;

	public bool twinShotModified = false;

	public float soundFireRateTime = 0.1f;
	private float soundFireTimer = 0f;

	public GameObject twinShotPrefab;
	private float forwardOffset = 4f;
	private int vIndex = 0;
	private int[] vPositions = new int[]{ -2, 2 };

	public GameObject backShotPrefab;
	private float backShotOffset = 6f;

	public GameObject snakePrefab;
	public GameObject snakeClonePrefab;
	public GameObject snakePulsePrefab;
	public GameObject hunterPrefab;

	public GameObject bladePrefab;
	public GameObject bladeMiniPrefab;
	private float miniBladeAngle = 15f;

	public GameObject railGunPrefab;
	private int railGunSegments = 5;
	private float railGunForwardOffset = 10f;
	private float lowRailGunSpeed = 300f;
	private float highRailGunSpeed = 550f;	//+150 speed

	public GameObject severPrefab;
	private float severForwardOffset = 10f;

	public GameObject explosionBombPrefab;
	public GameObject BFGPulsePrefab;

	public PlayerWeapon curWeapon = PlayerWeapon.Twinshot;
	public AudioClip[] fireSounds;

	public Vector2 savedInputSample = Vector2.right;
	public Vector2 inputSample;
	public float sampleCheckTime = 10/60f;
	private float sampleCheckTimer = 0f;

	void Awake() {
		playerMover = GetComponent<PlayerMovement>();
		modelCon = GetComponentInChildren<PlayerModelController>();
		clawCon = GetComponent<PlayerClawController>();
		differenceTracking = getCopyOfPlayerWeapons();
	}

	void Start() {
		SetCurrentWeapon();
		onWeaponsUpdated();

		//Create Bullet Pools
		foreach(PlayerWeapon weapon in Enum.GetValues(typeof(PlayerWeapon))){
			if(weapon != PlayerWeapon.None)
				ObjectPoolingManager.CreateNewObjectPool(weapon.ToString(), GetWeaponPrefab(weapon), 100);
		}

		//Create Extra Weapon Pools
		foreach(PlayerExtraWeapon eWeapon in Enum.GetValues(typeof(PlayerExtraWeapon))){
			ObjectPoolingManager.CreateNewObjectPool(eWeapon.ToString(), GetWeaponPrefab(eWeapon), 150);
		}

		/**
		ObjectPoolingManager.CreateNewObjectPool(PlayerWeapon.Twinshot.ToString(), twinShotPrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerWeapon.Backshot.ToString(), backShotPrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerWeapon.Snake.ToString(), snakePrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerExtraWeapon.SnakeClone.ToString(), snakeClonePrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerWeapon.Hunter.ToString(), hunterPrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerWeapon.Blade.ToString(), bladePrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerExtraWeapon.MiniBlade.ToString(), bladeMiniPrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerWeapon.Railgun.ToString(), railGunPrefab, 50);
		ObjectPoolingManager.CreateNewObjectPool(PlayerWeapon.Saber.ToString(), severPrefab, 50);
		*/
	}

	void Update () {
		if(playerWeaponsListChanged()) {
			differenceTracking = getCopyOfPlayerWeapons();
			scrollToAnotherAvailableWeapon();
			SetCurrentWeapon();
			onWeaponsUpdated();
		}

		if(Input.GetKeyDown(KeyCode.Alpha3))
			angledTwinShots = !angledTwinShots;

		if(Input.GetKeyDown(KeyCode.Alpha4))
			snakeModified = !snakeModified;

		if(Input.GetKeyDown(KeyCode.Alpha5))
			twinShotModified = !twinShotModified;

		if(cInput.GetKeyDown("SwitchWeapon"))
			GetNextWeapon();

		if (cInput.GetKey("Shoot") && shootingEnabled)
			FireWeapon();

		//Decrease Timers
		if(fireTimer > 0f)
			fireTimer -= Time.deltaTime;
		if(soundFireTimer > 0f)
			soundFireTimer -= Time.deltaTime;

		if(sampleCheckTimer < 0f)
			sampleCheckTimer -= Time.deltaTime;
		else {
			sampleCheckTimer = sampleCheckTime;
			inputSample = playerMover.inputVector;
		}
	}

	private List<PlayerWeapon> getCopyOfPlayerWeapons() {
		if(availableWeapons == null || availableWeapons.Count == 0) {
			availableWeapons = new List<PlayerWeapon>() { PlayerWeapon.None };
			return new List<PlayerWeapon>() { PlayerWeapon.None };
		}

		List<PlayerWeapon> newListWeapons = new List<PlayerWeapon>();
		foreach (PlayerWeapon w in availableWeapons) {
			newListWeapons.Add(w);
		}
		return newListWeapons;
	}

	private bool playerWeaponsListChanged() {
		if(differenceTracking.Count != availableWeapons.Count)
			return true;
		else {
			for(int i = 0; i < differenceTracking.Count; i++) {
				if(availableWeapons[i] != differenceTracking[i])
					return true;
			}
			return false;
		}
	}

	private bool areAvailableWeapons() {
		foreach(PlayerWeapon w in availableWeapons) {
			if(w != PlayerWeapon.None)
				return true;
		}
		return false;
	}

	public void scrollToAnotherAvailableWeapon() {
		int startingIndex = curWeaponIndex;

		//If there is a weapon on the starting index, select it.
		if (startingIndex >= 0 && startingIndex < availableWeapons.Count && availableWeapons[curWeaponIndex] != PlayerWeapon.None) {
			//Do nothing here. There is a weapon here already.
		}
		//Otherwise, the current index is off of the weapon bar or the weapon is unavailable.
		//Scroll to the left until we find a weapon.
		else {
			curWeaponIndex = 0;
			startingIndex = Mathf.Clamp(startingIndex, 0, availableWeapons.Count-1);
			int i = startingIndex;
			do{
				i = (i + availableWeapons.Count - 1) % availableWeapons.Count;
				if(availableWeapons[i] != PlayerWeapon.None) {
					curWeaponIndex = i;
					break;
				}

			} while (i != startingIndex);
		}

		//Set the current weapon
		curWeapon = availableWeapons[curWeaponIndex];
	}

	public void GetPreviousWeapon() {
		bool weaponsAvailable = areAvailableWeapons();

		//No weapons available. Pick the first one. It doesn't matter.
		if (!weaponsAvailable)
			curWeapon = availableWeapons[0];

		//Scroll backwards until a weapon is found
		else {
			do {
				curWeaponIndex = (curWeaponIndex + availableWeapons.Count - 1) % availableWeapons.Count;
			}
			while (availableWeapons[curWeaponIndex] == PlayerWeapon.None);
			curWeapon = availableWeapons[curWeaponIndex];
		}

		SwitchWeapon();
	}

	public void GetNextWeapon() {
		bool weaponsAvailable = areAvailableWeapons();

		//No weapons available. Pick the first one. It doesn't matter.
		if (!weaponsAvailable)
			curWeapon = availableWeapons[0];

		//Scroll forward until a weapon is found
		else {
			do {
				curWeaponIndex = (curWeaponIndex + 1) % availableWeapons.Count;
			}
			while (availableWeapons[curWeaponIndex] == PlayerWeapon.None);
			curWeapon = availableWeapons[curWeaponIndex];
		}

		SwitchWeapon();
	}

	public void SetCurrentWeapon() {
		clawCon.resetRotationSpeed();

		switch(curWeapon){
			case PlayerWeapon.Twinshot:
				fireRateTime = 0.04f;
				soundFireRateTime = 0.04f;
				fireSound.clip = fireSounds[0];
				break;

			case PlayerWeapon.Backshot:
				fireRateTime = 0.08f;
				soundFireRateTime = 0.125f;
				fireSound.clip = fireSounds[1];
				break;
			
			case PlayerWeapon.Snake:
				fireRateTime = 0.15f;
				soundFireRateTime = 0.25f;
				fireSound.clip = fireSounds[2];
				break;
			
			case PlayerWeapon.Freeway:
				fireRateTime = 0.12f;
				soundFireRateTime = 0.12f;
				fireSound.clip = fireSounds[3];
				break;
			
			case PlayerWeapon.Hunter:
				fireRateTime = 0.125f;
				soundFireRateTime = 0.125f;
				fireSound.clip = fireSounds[4];
				break;
			
			case PlayerWeapon.Blade:
				fireRateTime = 0.08f;
				soundFireRateTime = 0.12f;
				fireSound.clip = fireSounds[5];
				break;
			
			case PlayerWeapon.Railgun:
				fireRateTime = 0.12f;
				soundFireRateTime = 0.12f;
				fireSound.clip = fireSounds[6];
				break;

			case PlayerWeapon.Saber:
				fireRateTime = 0.12f;
				soundFireRateTime = 0.12f;
				fireSound.clip = fireSounds[7];
				break;
			
			case PlayerWeapon.Bomber:
				clawCon.setRotationSpeed(-3);
				fireRateTime = 0.75f;
				soundFireRateTime = 0.75f;
				//fireRateTime = 0.125f;
				//soundFireRateTime = 0.125f;
				fireSound.clip = fireSounds[8];
				break;

			default:
				//Nothing.
				fireSound.clip = null;
				break;
		}
	}

	public void SwitchWeapon() {
		SetCurrentWeapon();
		onWeaponSwitch();
	}

	public void FireWeapon() {
		if(fireTimer <= 0f) {
			switch(curWeapon){
				case PlayerWeapon.Twinshot:
					TwinShot();
					break;

				case PlayerWeapon.Backshot:
					BackShot();
					break;

				case PlayerWeapon.Snake:
					SnakeShot();
					break;
				
				case PlayerWeapon.Freeway:
					FreewayShot();
					break;

				case PlayerWeapon.Hunter:
					HunterShot();
					break;

				case PlayerWeapon.Blade:
					BladeShot();
					break;
				
				case PlayerWeapon.Railgun:
					RailGunShot();
					break;

				case PlayerWeapon.Saber:
					SeverShot();
					break;

				case PlayerWeapon.Bomber:
					SuperBombShot();
					break;

				default:
					//Nothing.
					break;
			}

			fireTimer = fireRateTime;
		}

		PlayFireSound();
	}

	public void PlayFireSound() {
		if(soundFireTimer <= 0f) {
			fireSound.Stop();
			fireSound.Play();
			soundFireTimer = soundFireRateTime;
		}
	}

	public string GetWeaponKey(PlayerWeapon weapon) {
		return weapon.ToString();
	}

	public string GetWeaponKey(PlayerExtraWeapon weapon) {
		return weapon.ToString();
	}

	public GameObject GetWeaponPrefab(PlayerWeapon weapon) {
		switch (weapon) {
			case PlayerWeapon.Twinshot:
				return twinShotPrefab;

			case PlayerWeapon.Backshot:
				return backShotPrefab;

			case PlayerWeapon.Snake:
				return snakePrefab;

			case PlayerWeapon.Freeway:
				return twinShotPrefab;

			case PlayerWeapon.Hunter:
				return hunterPrefab;

			case PlayerWeapon.Blade:
				return bladePrefab;

			case PlayerWeapon.Railgun:
				return railGunPrefab;

			case PlayerWeapon.Saber:
				return severPrefab;
			
			case PlayerWeapon.Bomber:
				return explosionBombPrefab;

			default:
				return null;
		}
	}

	public GameObject GetWeaponPrefab(PlayerExtraWeapon weapon) {
		switch (weapon) {
			case PlayerExtraWeapon.MiniBlade:
				return bladeMiniPrefab;

			case PlayerExtraWeapon.SnakeClone:
				return snakeClonePrefab;

			case PlayerExtraWeapon.SnakePulse:
				return snakePulsePrefab;

			case PlayerExtraWeapon.BFGPulse:
				return BFGPulsePrefab;

			default:
				return null;

		}
	}

	public void TwinShot() {
		int numShots = !angledTwinShots ? 2 : 1;
		for(int loop = 0; loop < numShots; loop++) {
			Vector3 forwardVector = twinShotModified ? modelCon.transform.forward : transform.up;
			ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Twinshot), transform.position + (transform.right * forwardOffset) + (forwardVector * vPositions[vIndex]), Quaternion.identity, transform.parent);
			vIndex = (vIndex + 1) % vPositions.Length;
		}

		foreach(Transform claw in clawCon.claws) {
			if(!angledTwinShots)
				ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Twinshot), claw.position + (transform.right * forwardOffset), Quaternion.identity, transform.parent);
			else {
				float angle = ((claw.position - transform.position).y > 0) ? twinShotAngle : -twinShotAngle;
				ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Twinshot), claw.position + (transform.right * forwardOffset), Quaternion.AngleAxis(angle, Vector3.forward), transform.parent);
			}
		}
	}

	public void BackShot() {
		ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Backshot), transform.position + (transform.right * backShotOffset), Quaternion.identity, transform.parent);
		ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Backshot), transform.position + (transform.right * -backShotOffset), Quaternion.AngleAxis(180, Vector3.up), transform.parent);

		foreach(Transform claw in clawCon.claws) {
			if(!angledTwinShots)
				ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Backshot), claw.position + (transform.right * -backShotOffset), Quaternion.AngleAxis(180, Vector3.up), transform.parent);
			else {
				float angle = ((claw.position - transform.position).y > 0) ? twinShotAngle : -twinShotAngle;
				ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Backshot), claw.position + (transform.right * -backShotOffset), Quaternion.AngleAxis(180, Vector3.up) * Quaternion.AngleAxis(angle, Vector3.forward), transform.parent);
			}
		}
	}

	public void SnakeShot() {
		ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Twinshot), transform.position + (transform.right * forwardOffset), Quaternion.identity, transform.parent);

		float forwardSpeed = 25;

		if(playerMover.inputVector.x != 0f) { 
			switch((int)Mathf.Sign(playerMover.inputVector.x)) {
				case -1:
					forwardSpeed = 5;
					break;

				case 1:
					forwardSpeed = 75;
					break;
			}
		}

		SnakeShot snake;
		GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Snake), transform.position, Quaternion.identity, transform.parent);
		if(bullet != null) {
			snake = bullet.GetComponent<SnakeShot>();
			snake.gravityDirection = 1;
			snake.bulletSpeed = forwardSpeed;
		}
		GameObject bullet2 = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Snake), transform.position, Quaternion.identity, transform.parent);
		if(bullet2 != null) {
			snake = bullet2.GetComponent<SnakeShot>();
			snake.gravityDirection = -1;
			snake.bulletSpeed = forwardSpeed;
		}
		foreach(Transform claw in clawCon.claws) {
			if(snakeModified) {
				ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerExtraWeapon.SnakePulse), claw.position + (transform.right * backShotOffset), Quaternion.identity, transform.parent);
				ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerExtraWeapon.SnakePulse), claw.position + (transform.right * -backShotOffset), Quaternion.AngleAxis(180, Vector3.up), transform.parent);
			}
			else {
				ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerExtraWeapon.SnakeClone), claw.position, Quaternion.identity, transform.parent);
			}
		}
	}

	public void FreewayShot() {
		if(inputSample != Vector2.zero)
			savedInputSample = inputSample;

		Quaternion startRot = Quaternion.AngleAxis(Mathf.Rad2Deg * Mathf.Atan2(savedInputSample.y, savedInputSample.x), Vector3.forward) * Quaternion.AngleAxis(180, Vector3.forward);

		for (int i = -2; i < 3; i++) {
			GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Twinshot), transform.position, startRot * Quaternion.AngleAxis(45/2f * i, Vector3.forward), transform.parent);
			if(bullet != null)
				bullet.transform.localPosition += bullet.transform.right * 5f;
		}

		GameObject bullet2 = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Twinshot), transform.position, startRot * Quaternion.AngleAxis(180, Vector3.forward), transform.parent);
		if(bullet2 != null)
				bullet2.transform.localPosition += bullet2.transform.right * 5f;

		foreach(Transform claw in clawCon.claws) {
			GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Backshot), claw.position, startRot, transform.parent);
			if(bullet != null)
				bullet.transform.localPosition += bullet.transform.right * 5f;
		}
	}

	public void HunterShot() {
		for(int i = -1; i <= 1; i++) {
			GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Hunter),transform.position, Quaternion.identity, transform.parent);
			if(bullet != null)
				bullet.GetComponent<HunterBullet>().directionVector = new Vector3(1, i, 0);
		}

		foreach(Transform claw in clawCon.claws) {
			GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Hunter), claw.position, Quaternion.identity, transform.parent);
		}
	}

	public void BladeShot() {
		ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Blade), transform.position + (transform.right * forwardOffset) + (transform.up * vPositions[vIndex]), Quaternion.identity, transform.parent);
		vIndex = (vIndex + 1) % vPositions.Length;

		for(int i = 0; i < clawCon.claws.Count; i++) {
			float angle = 0;
			if (clawCon.claws.Count > 1) {
				float i2 = i;
				float lerpFactor = i2/(clawCon.claws.Count-1);
				angle = Mathf.Lerp(-miniBladeAngle, miniBladeAngle, lerpFactor);
			}
			ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerExtraWeapon.MiniBlade), clawCon.claws[i].position + (transform.right * forwardOffset), Quaternion.AngleAxis(angle, Vector3.forward), transform.parent);
		}
	}

	public void RailGunShot() {
		ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Backshot), transform.position + (transform.right * backShotOffset), Quaternion.identity, transform.parent);

		for(int i = 0; i < railGunSegments; i++) {
			GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Railgun), transform.position + (transform.right * -railGunForwardOffset) + (transform.up * vPositions[vIndex]), Quaternion.AngleAxis(180, Vector3.up), transform.parent);
			if(bullet != null) {
				bullet.GetComponent<TwinShot>().bulletSpeed = Mathf.Lerp(lowRailGunSpeed, highRailGunSpeed, (float)i/railGunSegments);
				//bullet.transform.Translate(-bullet.transform.right * 1f * i);
			}
		}
		vIndex = (vIndex + 1) % vPositions.Length;

		for(int i = 0; i < clawCon.claws.Count; i++) {
			for(int i2 = 0; i2 < railGunSegments; i2++) {
				GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Railgun), clawCon.claws[i].position + (transform.right * -railGunForwardOffset), Quaternion.AngleAxis(180, Vector3.up), transform.parent);
				if(bullet != null) {
					bullet.GetComponent<TwinShot>().bulletSpeed = Mathf.Lerp(lowRailGunSpeed, highRailGunSpeed, (float)i2/railGunSegments);
					//bullet.transform.Translate(-bullet.transform.right * 1f * i2);
				}
			}
		}
	}

	public void SeverShot() {
		for(int i = 0; i < railGunSegments; i++) {
			GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Saber), transform.position + (transform.right * severForwardOffset) + (transform.up * vPositions[vIndex]), Quaternion.identity, transform.parent);
			if(bullet != null)
				bullet.GetComponent<TwinShot>().bulletSpeed = Mathf.Lerp(lowRailGunSpeed, highRailGunSpeed, (float)i/railGunSegments);
		}
		vIndex = (vIndex + 1) % vPositions.Length;

		for(int i = 0; i < clawCon.claws.Count; i++) {
			for(int i2 = 0; i2 < railGunSegments; i2++) {
				GameObject bullet = ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Saber), clawCon.claws[i].position + (transform.right * severForwardOffset), Quaternion.identity, transform.parent);
				if(bullet != null)
					bullet.GetComponent<TwinShot>().bulletSpeed = Mathf.Lerp(lowRailGunSpeed, highRailGunSpeed, (float)i2/railGunSegments);
			}
		}
	}

	public void SuperBombShot() {
		ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Bomber), transform.position + (transform.right * forwardOffset), Quaternion.identity, transform.parent);
	}

	private const int bomberCount = 3;
	private const float spread = 0.5f;
	public void TestBomberShot() {
		for(int i = 0; i < bomberCount; i++) {
			GameObject bomber =	ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Bomber), transform.position + (transform.right * forwardOffset), Quaternion.identity, transform.parent);
			CruiseMissile missile;
			if( (missile = bomber.GetComponent<CruiseMissile>()) != null ) {
				missile.setDirectionVector( new Vector3(1f, Mathf.Lerp(-spread, spread, i/(bomberCount-1f)), 0 ) );
			}
		}

		for(int i = 0; i < clawCon.claws.Count; i++) {
			GameObject bomber =	ObjectPoolingManager.GetObjectFromPool(GetWeaponKey(PlayerWeapon.Bomber), clawCon.claws[i].position, Quaternion.identity, transform.parent);
		}
	}

}
