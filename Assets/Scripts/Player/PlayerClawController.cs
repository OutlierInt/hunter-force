﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerClawController : MonoBehaviour {

	public GameObject clawPrefab;
	public List<Transform> claws;
	public int maxClawCount = 3;
	public float clawDistance = 8f;
	public float clawRotateSpeed = 1f;
	public float clawRotateSpeedGoal = -1f;
	public float clawRotateChangeSpeed = 1f;

	private float timer = 0;
	private const float twoPI = Mathf.PI * 2;
	private float initRotationSpeed;

	void Awake() {
		initRotationSpeed = clawRotateSpeed;
		clawRotateSpeedGoal = clawRotateSpeed;
	}

	public void setRotationSpeed(float newSpeed) {
		clawRotateSpeedGoal = newSpeed;
	}

	public void resetRotationSpeed() {
		clawRotateSpeedGoal = initRotationSpeed;
	}

	void Update() {
		clawRotateSpeed = Mathf.Lerp(clawRotateSpeed, clawRotateSpeedGoal, Time.deltaTime * clawRotateChangeSpeed);

		timer = (timer + Time.deltaTime * clawRotateSpeed) % 1f;

		if(Input.GetKeyDown(KeyCode.Alpha1))
			AddClaw();

		if(Input.GetKeyDown(KeyCode.Alpha2))
			RemoveClaw();

		if(claws.Count > 0) {
			float clawAngleOffset = twoPI / claws.Count;

			for(int i = 0; i < claws.Count; i++) {
				Vector3 rotOffset = new Vector2(
					Mathf.Cos(twoPI * timer + clawAngleOffset * i),
					Mathf.Sin(twoPI * timer + clawAngleOffset * i));

				rotOffset = transform.TransformVector(rotOffset);
				claws[i].position = transform.position + (rotOffset * clawDistance);
				claws[i].rotation = Camera.main.transform.rotation;
			}
		}
	}

	public void AddClaw() {
		if(claws.Count < maxClawCount) {
			GameObject claw = Instantiate(clawPrefab, transform.position, Quaternion.identity) as GameObject;
			claws.Add(claw.transform);
		}
	}

	public void RemoveClaw() {
		if(claws.Count > 0) {
			int i = claws.Count - 1;
			Destroy(claws[i].gameObject);
			claws.RemoveAt(i);
		}
	}
	
}
