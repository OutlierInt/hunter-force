﻿using UnityEngine;
using System.Collections;

public class PlayerShootDownAnimation : MonoBehaviour {

	public PlayerModelController modelController;
	public ParticleSystem[] particleSprays;
	public bool deathAnimationActive = false;
	public Vector3 fallOffset = new Vector3(-30, -50, 0);
	public Vector3 initTangentOffset, fallOffsetTangentOffset;
	public float fallTime = 1f;
	public float spinSpeed = 10f;
	public float minForwardSpeedMultiplier, maxForwardSpeedMultiplier;
	//public float explosionTime = 0.1f;
	//private float explosionTimer = 0;
	private Coroutine fallingCoroutine;

	void Update() {
		//Spray Particles from ship
		foreach(ParticleSystem pSystem in particleSprays) {
			var emission = pSystem.emission;
			emission.enabled = deathAnimationActive;
		}
	}
	
	public void DeathAnimation() {
		deathAnimationActive = true;
		ImpactEffectManager.main.CreatePlayerExplosion(transform.position);
		fallingCoroutine = StartCoroutine(fallDown());
		modelController.enabled = false;
	}

	public void ResetPlayer() {
		if(fallingCoroutine != null)
			StopCoroutine(fallingCoroutine);
		modelController.enabled = true;
		resetShip();
		deathAnimationActive = false;
	}

	private void resetShip() {
		modelController.transform.position = transform.position;
		modelController.transform.localRotation = Quaternion.identity;
	}

	private IEnumerator fallDown() {
		float timer = 0;

		//Modify death animation
		float xModifier = Random.Range(minForwardSpeedMultiplier, maxForwardSpeedMultiplier) * (transform.localPosition.x <= 0 ? -1 : 1);
		Vector3 modifiedFallOffset = fallOffset;
		Vector3 modifiedInitTangent = initTangentOffset;
		modifiedFallOffset.x *= xModifier;
		modifiedInitTangent.x *= xModifier;

		Vector3 spinAxis = Random.insideUnitSphere.normalized;
		if(spinAxis == Vector3.zero)
			spinAxis = transform.forward;

		//Animate
		while(timer < fallTime) {
			timer += Time.deltaTime;
			modelController.transform.localPosition = 
				CatmullRom.evaluate(Vector3.zero + modifiedInitTangent, Vector3.zero, modifiedFallOffset, modifiedFallOffset + fallOffsetTangentOffset, timer/fallTime);
			modelController.transform.Rotate(spinAxis, spinSpeed * Time.deltaTime, Space.Self);
			yield return new WaitForEndOfFrame();
		}

		deathAnimationActive = false;
	}
}
