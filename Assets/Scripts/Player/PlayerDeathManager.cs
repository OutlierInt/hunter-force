﻿using UnityEngine;
using System.Collections;

public class PlayerDeathManager : MonoBehaviour {

	public bool dead = false;
	private Collider playerHitVolume;
	private PlayerMovement mover;
	private PlayerShooting shooter;
	private PlayerShootDownAnimation shootDownAnimation;
	private PlayerBomber bomber;

	void Awake() {
		playerHitVolume = GetComponent<Collider>();
		mover = GetComponent<PlayerMovement>();
		shooter = GetComponent<PlayerShooting>();
		shootDownAnimation = GetComponent<PlayerShootDownAnimation>();
		bomber = GetComponent<PlayerBomber>();
	}

	void Update() {
		if(Input.GetKeyDown(KeyCode.Q))
			KillPlayer();
		if(Input.GetKeyDown(KeyCode.W))
			Respawn();
	}

	void OnCollisionEnter(Collision col) {
		if(col.gameObject.CompareTag("EnemyBullet"))
			KillPlayer();
	}

	void KillPlayer() {
		if(!dead) {
			dead = true;
			playerHitVolume.enabled = false;
			shooter.shootingEnabled = false;
			mover.movementEnabled = false;
			bomber.enabled = false;
			shootDownAnimation.DeathAnimation();
		}
	}

	void Respawn() {
		if(dead) {
			dead = false;
			playerHitVolume.enabled = true;
			shooter.shootingEnabled = true;
			mover.movementEnabled = true;
			bomber.enabled = true;
			shootDownAnimation.ResetPlayer();
		}
	}
	
}
