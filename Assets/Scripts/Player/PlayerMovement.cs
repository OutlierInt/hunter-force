﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public delegate void EventAction();
	public static event EventAction onSpeedChange;

	private GameManager manager;

	public bool movementEnabled = true;
	//public bool movementEnabled = true;

	public float minSpeed, maxSpeed;
	public float moveSpeed = 60f;
	private const float minSpeedLevel = 1f;
	private const float maxSpeedLevel = 4f;
	private const float speedLevelIncrement = 1f;
	private float speedPercentage = 3f;

	[HideInInspector]
	public Vector3 inputVector;

	public float preciseSpeedAdjustThrehold = 0.5f;
	public float preciseSpeedAdjustSpeed = 1f;
	private float buttonHeldDownTimer = 0f;

	void Awake() {
		manager = GameManager.main;
	}

	void Start() {
		updatePlayerSpeedVariables();
	}

	void Update() {
		//Change Speed
		if(cInput.GetKeyDown("SpeedChange"))
			adjustPlayerSpeedLarge();

		if(cInput.GetKey("SpeedChange")) {
			if(buttonHeldDownTimer < preciseSpeedAdjustThrehold)
				buttonHeldDownTimer += Time.unscaledDeltaTime;
			else
				adjustPlayerSpeedPrecisely();
		}
		else
			buttonHeldDownTimer = 0f;


		//Get Inputs
		inputVector = new Vector3(
			cInput.GetAxis("Horizontal"),
			cInput.GetAxis("Vertical"),
			0f);

		//Clamp so speed isn't 40% faster on diagonals
		inputVector = Vector3.ClampMagnitude(inputVector, 1f);

		if(movementEnabled) {
			//Move player about!
			transform.localPosition += inputVector * moveSpeed * Time.deltaTime;

			//Clamp Player's position to play area
			transform.localPosition = new Vector3(
				Mathf.Clamp(transform.localPosition.x, manager.gameplayBoundsHorizontal.x, manager.gameplayBoundsHorizontal.y),
				Mathf.Clamp(transform.localPosition.y, manager.gameplayBoundsVertical.x, manager.gameplayBoundsVertical.y),
				0f);
		}
	}

	public void adjustPlayerSpeedLarge() {
		//Round speed to nearest int, then increment
		speedPercentage = (int)speedPercentage + speedLevelIncrement;

		updatePlayerSpeedVariables();
		onSpeedChange();
	}

	public void adjustPlayerSpeedPrecisely() {
		//Update slowly
		speedPercentage += preciseSpeedAdjustSpeed * Time.unscaledDeltaTime;

		updatePlayerSpeedVariables();
		onSpeedChange();
	}

	public void updatePlayerSpeedVariables() {
		speedPercentage = speedPercentage > maxSpeedLevel ? minSpeedLevel : speedPercentage;
		moveSpeed = Mathf.Lerp(minSpeed, maxSpeed, getSpeedPercentage());
	}

	public float getSpeedPercentage() {
		return Mathf.InverseLerp(minSpeedLevel, maxSpeedLevel, speedPercentage);
	}

}
