﻿using UnityEngine;
using System.Collections;

public class Flash : MonoBehaviour {
	private Renderer render;
	private float timer = 0;

	public float flashRate = 3;

	void Awake() {
		render = GetComponent<Renderer>();
	}

	void LateUpdate() {
		if(timer <= 0) {
			render.enabled = !render.enabled;
			timer = 1/flashRate - Time.deltaTime;
		}
		else
			timer -= Time.deltaTime;
	}
}
