﻿using UnityEngine;
using System.Collections;

public enum ImpactEffectType { HunterImpact, SmallExplosion, HitFlash, LargeExplosion, MediumExplosion, BFGExplosion }
public class ImpactEffectManager : MonoBehaviour {

	public static ImpactEffectManager main;
	public ParticleSystem hunterImpactEffect;
	public ParticleSystem smallHitConfirmEffect;

	//Colors
	public Color hunterHitColor = Color.white;
	public Color hunterFailColor = Color.grey;
	public Color smallHitConfirmColor = Color.red;
	public Color smallHitFailColor = Color.grey;

	//Particle Effects
	public GameObject smallExplosionPrefab;
	public GameObject mediumExplosionPrefab;
	public GameObject largeExplosionPrefab;
	public GameObject playerExplosionPrefab;
	public GameObject BFGExplosionPrefab;

	public const string SMALLEXP = "SmallExplosion";
	public const string MEDIUMEXP = "MediumExplosion";
	public const string LARGEEXP = "LargeExplosion";
	public const string BFGEXP = "BFGExplosion";

	void Awake() {
		main = this;
	}

	void Start() {
		ObjectPoolingManager.CreateNewObjectPool(SMALLEXP, smallExplosionPrefab, 100);
		ObjectPoolingManager.CreateNewObjectPool(MEDIUMEXP, mediumExplosionPrefab, 100);
		ObjectPoolingManager.CreateNewObjectPool(LARGEEXP, largeExplosionPrefab, 100);
	}

	public void CreateImpactEffect(ImpactEffectType effect, Vector3 position, bool hitSuccess = true) {
		switch (effect) {
			case ImpactEffectType.HitFlash:
				CreateSmallHitFlash(position, hitSuccess);
				break;

			case ImpactEffectType.HunterImpact:
				CreateHunterImpact(position, hitSuccess);
				break;

			case ImpactEffectType.SmallExplosion:
				CreateSmallExplosion(position);
				break;

			case ImpactEffectType.MediumExplosion:
				CreateMediumExplosion(position);
				break;

			case ImpactEffectType.LargeExplosion:
				CreateLargeExplosion(position);
				break;

			case ImpactEffectType.BFGExplosion:
				CreateBFGExplosion(position);
				break;

			default:
				//Do nothing
				break;
		}
	}

	public void CreateHunterImpact(Vector3 position, bool hitSuccessful) {
		ParticleSystem.EmitParams param = new ParticleSystem.EmitParams();
		param.position = position;
		param.startColor = hitSuccessful ? hunterHitColor : hunterFailColor;
		hunterImpactEffect.Emit(param, 1);
	}

	public void CreateSmallHitFlash(Vector3 position, bool hitSuccessful) {
		ParticleSystem.EmitParams param = new ParticleSystem.EmitParams();
		param.position = position;
		param.startColor = hitSuccessful ? smallHitConfirmColor : smallHitFailColor;
		smallHitConfirmEffect.Emit(param, 1);
	}

	public void CreateSmallExplosion(Vector3 position) {
		ObjectPoolingManager.GetObjectFromPool(SMALLEXP, position, Quaternion.identity, transform);
	}

	public void CreateMediumExplosion(Vector3 position) {
		ObjectPoolingManager.GetObjectFromPool(MEDIUMEXP, position, Quaternion.identity, transform);
	}

	public void CreateLargeExplosion(Vector3 position) {
		ObjectPoolingManager.GetObjectFromPool(LARGEEXP, position, Quaternion.identity, transform);
	}

	public void CreatePlayerExplosion(Vector3 position) {
		Instantiate(playerExplosionPrefab, position, Quaternion.identity, transform);
	}

	public void CreateBFGExplosion(Vector3 position) {
		Instantiate(BFGExplosionPrefab, position, Quaternion.identity, transform);
	}

}
