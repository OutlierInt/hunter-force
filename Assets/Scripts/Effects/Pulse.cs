﻿using UnityEngine;
using System.Collections;

public class Pulse : MonoBehaviour {

	public Transform objectToPulse;
	private Vector3 initScale;
	public float minScale, maxScale;
	public float pulseRate = 1;
	private float timer = 0;

	void Awake() {
		if(objectToPulse != null)
			objectToPulse = transform;
		initScale = objectToPulse.localScale;
	}

	void LateUpdate() {
		objectToPulse.localScale = 
			initScale * Mathf.Lerp(
				minScale,
				maxScale,
				Mathf.InverseLerp(0, 1f/pulseRate, Mathf.PingPong(Time.time, 1f/pulseRate)));
	}
}
