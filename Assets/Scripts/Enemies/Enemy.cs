﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public abstract class Enemy : MonoBehaviour, IDamageable, IKillable {
	public int health = 100;
	public int pointValue = 100;

	private bool damagable = true;
	private bool killable = true;

	public virtual bool Damagable {
		get { return damagable; }
		set { damagable = value; }
	}
	public virtual bool Killable {
		get { return killable; }
		set { killable = value; }
	}

	void OnEnable() {
		EnemyManager.AddEnemyToManager(this);
	}

	void OnDisable() {
		EnemyManager.RemoveEnemyFromManager(this);
	}

	public virtual void Damage(int damageAmount) {
		if(damagable)
			health -= damageAmount;
	}

	public virtual void Kill() {
		if(killable)
			Destroy(gameObject);
	}

	public virtual void Update() {
		//Kill on health lost
		if(health <= 0 && Killable) {
			AwardPoints();
			Kill();
		}
	}

	public void AwardPoints() {
		ScoringManager.AddPoints(pointValue);
	}

	void OnCollisionEnter(Collision col) {
		if(col.gameObject.CompareTag(PlayerShooting.PLAYERWEAPONTAG)) {
			PlayerBullet bullet = col.gameObject.GetComponent<PlayerBullet>();
			if(bullet != null) {
				Damage(bullet.damage);
			}
		}
	}
}

public interface IDamageable {
	bool Damagable {
		get;
		set;
	}
	void Damage(int damageAmount);
}

public interface IKillable {
	bool Killable {
		get;
		set;
	}
	void Kill();
}