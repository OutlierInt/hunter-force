﻿using UnityEngine;
using System.Collections;
using System;

public class SimpleEnemy : Enemy{

	public float bulletSpeed = 50;
	public Vector3 localShootPosition;
	public float shootTime = Mathf.Infinity;
	private float shootTimer = Mathf.Infinity;

	void Awake() {
		shootTimer = shootTime;
	}

	public override void Update() {
		base.Update();

		//Shoot at player at regular intervals
		if(shootTimer <= 0) {
			ShootAtPlayer();
			shootTimer = shootTime;
		}
		else
			shootTimer -= Time.deltaTime;
	}

	public void ShootAtPlayer() {
		EnemyBulletManager.FireGenericBulletAt(transform.position + transform.TransformVector(localShootPosition), GameManager.PlayerPosition() /*transform.position + transform.right*/, bulletSpeed, transform.parent);
	}

	public override void Kill() {
		base.Kill();
		ImpactEffectManager.main.CreateMediumExplosion(transform.position);
	}

}
