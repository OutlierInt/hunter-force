﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour {

	public static List<Enemy> activeEnemies = new List<Enemy>();
	
	public static void AddEnemyToManager(Enemy enemy) {
		activeEnemies.Add(enemy);
	}

	public static void RemoveEnemyFromManager(Enemy enemy) {
		activeEnemies.Remove(enemy);
	}
}
