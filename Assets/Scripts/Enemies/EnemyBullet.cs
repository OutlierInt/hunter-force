﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour {

	void OnEnable() {
		EnemyBulletManager.AddBulletToManager(gameObject);
	}

	void OnDisable() {
		EnemyBulletManager.RemoveBulletFromManager(gameObject);
	}
}
