﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EnemyBulletManager : MonoBehaviour {

	public static List<GameObject> activeBullets = new List<GameObject>();

	public BulletEntry[] bullets;
	private static Quaternion transformLookRot = Quaternion.FromToRotation(Vector3.forward, Vector3.right);

	void Awake() {
		if(activeBullets != null)
			activeBullets.Clear();

		activeBullets = new List<GameObject>();
	}

	void Start() {
		foreach(BulletEntry entry in bullets) {
			ObjectPoolingManager.CreateNewObjectPool(entry.name, entry.bulletPrefab, entry.count);
		}
	}

	public static GameObject FireGenericBulletAt(Vector3 origin, Vector3 target, float speed, Transform parent) {
		GameObject bullet = FireBulletAt("Generic", origin, target, parent);
		bullet.GetComponent<TwinShot>().bulletSpeed = speed;
		return bullet;
	}

	public static GameObject FireGenericBullet(Vector3 position, Quaternion rotation, Transform parent) {
		return ObjectPoolingManager.GetObjectFromPool("Generic", position, rotation, parent);
	}

	public static GameObject FireBulletAt(string bulletKey, Vector3 origin, Vector3 target, Transform parent) {
		GameObject bullet = ObjectPoolingManager.GetObjectFromPool(bulletKey, origin, Quaternion.identity, parent);
		if(bullet != null) {
			Vector3 aimDirection = origin - target;
			if(aimDirection != Vector3.zero)
				bullet.transform.rotation = Quaternion.LookRotation(aimDirection, GameManager.main.gameplayPlane.right) * transformLookRot;
		}
		return bullet;
	}

	public static void AddBulletToManager(GameObject enemyBullet) {
		activeBullets.Add(enemyBullet);
	}

	public static void RemoveBulletFromManager(GameObject enemyBullet) {
		activeBullets.Remove(enemyBullet);
	}
}
[System.Serializable]
public class BulletEntry {
	public string name;
	public GameObject bulletPrefab;
	public int count;
}
