﻿using UnityEngine;
using System.Collections;

public class VirtualControls : MonoBehaviour {
	
	public static bool defaultControlsSet = false;
	public static float sensitivity = 1000f;
	public static float gravity = 1000f;
	public static float deadzone = 0.15f;

	void Awake() {
		if(!defaultControlsSet)
			SetDefaultPlayerControls();
	}

	public static void SetDefaultPlayerControls() {
		cInput.SetKey("Up", Keys.UpArrow);
		cInput.SetKey("Down", Keys.DownArrow);
		cInput.SetAxis("Vertical", "Down", "Up", sensitivity, gravity, deadzone);

		cInput.SetKey("Left", Keys.LeftArrow);
		cInput.SetKey("Right", Keys.RightArrow);
		cInput.SetAxis("Horizontal", "Left", "Right", sensitivity, gravity, deadzone);

		cInput.SetKey("SpeedChange", Keys.LeftShift);

		cInput.SetKey("Shoot", Keys.LeftControl);

		cInput.SetKey("SwitchWeapon", Keys.LeftAlt);

		cInput.SetKey("Bomb", Keys.Space);

		defaultControlsSet = true;
	}
}
