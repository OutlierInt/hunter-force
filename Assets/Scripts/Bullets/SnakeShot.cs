﻿using UnityEngine;
using System.Collections;

public class SnakeShot : MonoBehaviour {

	public float bulletSpeed = 10f;
	public float gravitySpeed = 0;
	public float gravityRate = 10f;
	public int gravityDirection = 1;

	void OnEnable() {
		gravitySpeed = 5f * gravityDirection;
	}

	void OnDisable() {
		gravitySpeed = 0;
	}

	void Update() {
		gravitySpeed += Time.deltaTime * gravityDirection * gravityRate;

		transform.position += transform.right * bulletSpeed * Time.deltaTime;
		transform.position += transform.up * gravitySpeed * Time.deltaTime;
	}
}
