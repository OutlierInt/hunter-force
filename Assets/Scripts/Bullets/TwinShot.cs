﻿using UnityEngine;
using System.Collections;

public class TwinShot : MonoBehaviour {

	public float bulletSpeed = 10f;

	void Update() {
		transform.position += transform.right * bulletSpeed * Time.deltaTime;
	}

}
