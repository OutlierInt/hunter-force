﻿using UnityEngine;
using System.Collections;

public class AimBullet : MonoBehaviour {

	public float dischargeTime = 0.1f;
	public float dischargeSpeed = 100f;
	public float range = 10;
	//public float bulletCancelRange = 10;
	//public float bulletCancelRangeDischargeSpeed = 100f;

	void OnEnable() {
		InvokeRepeating("Discharge", 0, dischargeTime);
	}

	void OnDisable() {
		ImpactEffectManager.main.CreateSmallExplosion(transform.position);
		CancelInvoke();
	}

	void Discharge() {
		float trueRange = range * range;

		//Discharge at enemies
		foreach (Enemy enemy in EnemyManager.activeEnemies) {
			if(Vector3.SqrMagnitude(enemy.transform.position - transform.position) <= trueRange) {
				GameObject bullet = EnemyBulletManager.FireBulletAt(PlayerExtraWeapon.BFGPulse.ToString(), transform.position, enemy.transform.position, transform.parent);
				if(bullet != null)
					bullet.GetComponent<TwinShot>().bulletSpeed = dischargeSpeed;
			}
		}

		
		/** Discharging at bullets sounds good, but doesn't work that well in practice.
		//Discharge at bullets
		float trueBulletCancelRange = bulletCancelRange * bulletCancelRange;
		foreach (GameObject eBullet in EnemyBulletManager.activeBullets) {
			if(Vector3.SqrMagnitude(eBullet.transform.position - transform.position) <= trueBulletCancelRange) {
				Vector3 aimDirection = eBullet.transform.position - transform.position;
				GameObject bullet = EnemyBulletManager.FireBulletAt(PlayerExtraWeapon.BFGPulse.ToString(), transform.position + aimDirection.normalized * transform.localScale.x * 0.5f, eBullet.transform.position, transform.parent);
				if(bullet != null) {
					bullet.GetComponent<TwinShot>().bulletSpeed = bulletCancelRangeDischargeSpeed;
					ImpactEffectManager.main.CreateImpactEffect(ImpactEffectType.HitFlash, bullet.transform.position, false);
				}
			}
		}
		*/


	}

}
