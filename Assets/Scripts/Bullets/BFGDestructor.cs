﻿using UnityEngine;
using System.Collections;

public class BFGDestructor : BulletDestructor {
	public int screenClearingDamage = 500;

	protected override void OnCollisionEnter(Collision col) {
		if(!col.gameObject.CompareTag("EnemyBullet")) {
			base.OnCollisionEnter(col);

			//Clear all bullets!
			ClearAllBullets();

			//Destroy Everything!	
			DamageAllEnemies();
		}
	}

	void DamageAllEnemies() {
		//foreach (Enemy enemy in EnemyManager.activeEnemies) {
		for(int i = 0; i < EnemyManager.activeEnemies.Count; i++) {
			Enemy enemy = EnemyManager.activeEnemies[i];
			ImpactEffectManager.main.CreateImpactEffect(ImpactEffectType.MediumExplosion, enemy.transform.position, enemy.Damagable);
			enemy.Damage(screenClearingDamage);
		}
	}

	void ClearAllBullets() {
		//foreach (GameObject eBullet in EnemyBulletManager.activeBullets) {
		//for(int i = 0; i < EnemyBulletManager.activeBullets.Count; i++) {
		while(EnemyBulletManager.activeBullets.Count > 0) {
			GameObject eBullet = EnemyBulletManager.activeBullets[0];
			ImpactEffectManager.main.CreateImpactEffect(ImpactEffectType.HitFlash, eBullet.transform.position, false);
			eBullet.SetActive(false);
		}
	}

}
