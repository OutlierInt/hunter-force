﻿using UnityEngine;
using System.Collections;

public class HunterManager : MonoBehaviour {

	public static Transform globalHunterTarget;
	public Transform hunterTarget;

	void Update() {
		if(hunterTarget != null)
			globalHunterTarget = hunterTarget;
		else {
			float distance = Mathf.Infinity;
			foreach(Enemy e in EnemyManager.activeEnemies) {
				if(e == null) continue;

				float distanceToEnemy = Vector3.SqrMagnitude(transform.position - e.transform.position);
				if(distance > distanceToEnemy) {
					distance = distanceToEnemy;
					globalHunterTarget = e.transform;
				}
			}
		}

	}

}
