﻿using UnityEngine;
using System.Collections;

public enum ImpactEffectLocation { Bullet, ContactPoint }
public class BulletDestructor : MonoBehaviour {

	public float lifeTime = 3f;
	public ImpactEffectLocation impactLocationType;
	public ImpactEffectType impactEffectType;

	protected virtual void OnEnable() {
		//Destroy(gameObject, lifeTime);
		Invoke("Destroy", lifeTime);
	}

	protected virtual void OnBecameInvisible() {
		Destroy();
	}

	protected virtual void OnDisable() {
		CancelInvoke();
	}

	protected virtual void Destroy() {
		gameObject.SetActive(false);
	}

	protected virtual void OnCollisionEnter(Collision col) {
		Vector3 impactLocation = 
			impactLocationType == ImpactEffectLocation.ContactPoint ?
			col.contacts[0].point :
			transform.position;

		ImpactEffectManager.main.CreateImpactEffect(impactEffectType, impactLocation);

		//Destroy(gameObject);
		Destroy();
	}
}
