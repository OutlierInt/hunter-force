﻿using UnityEngine;
using System.Collections;

public class HunterBullet : MonoBehaviour {

	public float homingSpeed = 1;
	public float speed = 10;
	public Vector3 directionVector = Vector3.right;

	void OnDisable() {
		directionVector = Vector3.right;
	}

	void Update() {
		if(HunterManager.globalHunterTarget != null) {
			Vector3 goalPosition = HunterManager.globalHunterTarget.position;
			Vector3 directionToGoal = goalPosition - transform.position;
			Vector3 directionToGoalNorm = transform.InverseTransformVector(directionToGoal.normalized);

			directionVector.x = Mathf.Lerp(directionVector.x, Mathf.Sign(directionToGoalNorm.x), Time.deltaTime * homingSpeed);
			directionVector.y = Mathf.Lerp(directionVector.y, Mathf.Sign(directionToGoalNorm.y), Time.deltaTime * homingSpeed);
			directionVector.z = Mathf.Lerp(directionVector.z, Mathf.Sign(directionToGoalNorm.z), Time.deltaTime * homingSpeed);

			directionVector.x = Mathf.Clamp(directionVector.x, -1f, 1f);
			directionVector.y = Mathf.Clamp(directionVector.y, -1f, 1f);
			directionVector.z = Mathf.Clamp(directionVector.z, -1f, 1f);

		}
		else {
			directionVector = Vector3.Lerp(directionVector, directionVector.normalized, Time.deltaTime * homingSpeed);
		}

		directionVector = Vector3.ClampMagnitude(directionVector, 1f);
		Debug.DrawLine(transform.position, transform.position + directionVector * 10f, Color.white);

		transform.localPosition += (directionVector * speed * Time.deltaTime);
	}
}
