﻿using UnityEngine;
using System.Collections;

public class HunterDestructor : BulletDestructor {

	public float lifeTimeNoTarget = 1f;
	public float lifeTimer = 0;
	void Update() {
		if(HunterManager.globalHunterTarget == null) {
			lifeTimer += Time.deltaTime;
			if(lifeTimer >= lifeTimeNoTarget) {
				ImpactEffectManager.main.CreateImpactEffect(ImpactEffectType.HunterImpact, transform.position, false);
				Destroy();
			}
		}
		else
			lifeTimer = 0;
	}

	protected override void OnEnable() {
		base.OnEnable();
		lifeTimer = 0;
	}

	protected override void OnDisable() {
		base.OnDisable();
		lifeTimer = 0;
	}

}
