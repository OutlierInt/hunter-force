﻿using UnityEngine;
using System.Collections;

public class CruiseMissile : MonoBehaviour {

	public float maxRotationDegreesPerSecond = 90f;
	public float speed = 10;
	public float minAccuracy = 5f;
	public Vector3 directionVector = Vector3.right;
	public float velocityChangeSpeed = 1f;
	public float slowDownRate = 3f;
	public float speedUpRate = 3f;
	[Range(0f, 1f)]
	public float minimumSlowScale = 0.15f;
	private float speedScale = 1;
	private Vector3 velocityDirection = Vector3.right;

	void OnDisable() {
		setDirectionVector(Vector3.right);
	}

	void Update() {
		Vector3 goalPosition, directionToGoal, directionToGoalNorm;

		if(HunterManager.globalHunterTarget != null) {
			goalPosition = HunterManager.globalHunterTarget.position;
			directionToGoal = goalPosition - transform.position;
			directionToGoalNorm = transform.InverseTransformVector(directionToGoal.normalized);

			directionVector = Vector3.RotateTowards(directionVector, directionToGoalNorm, maxRotationDegreesPerSecond * Mathf.Deg2Rad * Time.deltaTime, 0f);

			/*
			if (Vector3.Angle(directionVector, directionToGoalNorm) < minAccuracy)
				speedScaler = Mathf.Lerp(speedScaler, 1f, Time.deltaTime * speedUpRate);
			else
				speedScaler = Mathf.Lerp(speedScaler, minimumSlowScale, Time.deltaTime * slowDownRate);

			directionVector = Vector3.ClampMagnitude(directionVector, 1f);

			float t = Mathf.InverseLerp( minimumSlowScale, 1f, speedScaler );
			t *= Mathf.Max( minimumSlowScale, Mathf.Abs( Vector3.Dot( velocityDirection, directionVector ) ) );
			*/

			velocityDirection = Vector3.Lerp(velocityDirection, directionVector, Time.deltaTime * velocityChangeSpeed );
			velocityDirection = Vector3.ClampMagnitude(velocityDirection, 1f);

			if (Vector3.Angle(velocityDirection, directionToGoalNorm) < minAccuracy)
				speedScale = Mathf.Lerp(speedScale, 1f, Time.deltaTime * speedUpRate);
			else
				speedScale = Mathf.Lerp(speedScale, minimumSlowScale, Time.deltaTime * slowDownRate);

			Debug.DrawLine(transform.position, transform.position + directionVector * 10f, Color.white);
			Debug.DrawLine(transform.position, transform.position + velocityDirection * 10f, Color.green);

			transform.localPosition += (velocityDirection * speed * speedScale * Time.deltaTime);

		}
		else {
			transform.localPosition += (velocityDirection * speed * Time.deltaTime);
		}

	}

	public void setDirectionVector(Vector3 newDirection) {
		directionVector = newDirection;
		velocityDirection = newDirection;
	}
}
