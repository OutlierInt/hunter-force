﻿using UnityEngine;
using System.Collections;

public class CatmullRom : MonoBehaviour {

	public static float evaluateAngle (float f0, float f1, float f2, float f3, float t){
	
		float a;
		float b;
		float c;
		float d;

		Vector2 abWrap = wrapAround(f0,f1);
		a = abWrap.x;
		b = abWrap.y;

		Vector2 bcWrap = wrapAround(b,f2);
		b = bcWrap.x;
		c = bcWrap.y;

		Vector2 cdWrap = wrapAround(c,f3);
		c = cdWrap.x;
		d = cdWrap.y;

		return evaluate(a, b, c, d, t);
	}

	public static Vector2 wrapAround (float a, float b){
		//Distance to far. Need to go the other way
		if( Mathf.Abs(a - b) > 180){
			//Add 360 to the smaller value
			if( a > b )
				return new Vector2( a + 360, b );
			else
				return new Vector2( a, b + 360 );
		}
		//Distance is ok! Keep them the same.
		else
			return new Vector2(a, b);
	}

	public static float evaluate (float v0, float v1, float v2, float v3, float t){
		float t2 = t * t;
		float t3 = t2 * t;
		float a = 0.5f * (2.0f * v1);
		float b = 0.5f * (-v0 + v2);
		float c = 0.5f * (2.0f * v0 - 5.0f * v1 + 4 * v2 - v3);
		float d = 0.5f * (-v0 + 3.0f * v1 - 3.0f * v2 + v3);
		
		return a + b*t + c*t2 + d*t3;
	}

	public static Vector3 evaluate (Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3, float t){
		float t2 = t * t;
		float t3 = t2 * t;
		Vector3 a = 0.5f * (2.0f * v1);
		Vector3 b = 0.5f * (-v0 + v2);
		Vector3 c = 0.5f * (2.0f * v0 - 5.0f * v1 + 4 * v2 - v3);
		Vector3 d = 0.5f * (-v0 + 3.0f * v1 - 3.0f * v2 + v3);

		return a + b*t + c*t2 + d*t3;
	}
	
	public static Vector3 evaluateTangent (Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3, float t){
		float t2 = t * t;
		Vector3 b = 0.5f * (-v0 + v2);
		Vector3 c = 0.5f * (2.0f * v0 - 5.0f * v1 + 4 * v2 - v3);
		Vector3 d = 0.5f * (-v0 + 3.0f * v1 - 3.0f * v2 + v3);
		
		return b + 2*c*t + 3*d*t2;
	}

	public static Vector3 evaluateCurvature (Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3, float t){
		Vector3 c = 0.5f * (2.0f * v0 - 5.0f * v1 + 4 * v2 - v3);
		Vector3 d = 0.5f * (-v0 + 3.0f * v1 - 3.0f * v2 + v3);
		
		return 2*c + 6*d*t;
	}
}