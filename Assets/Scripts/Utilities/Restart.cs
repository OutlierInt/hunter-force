﻿using UnityEngine;
using System.Collections;

public class Restart : MonoBehaviour {

	public KeyCode RestartKey = KeyCode.R;

	void Update () {
		if(Input.GetKeyDown(RestartKey))
			RestartLevel();
	}

	public static void RestartLevel() {
		ObjectPoolingManager.ClearAllPools();
		Application.LoadLevel(Application.loadedLevel);
	}
}
