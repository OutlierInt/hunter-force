﻿using UnityEngine;
using System.Collections;

public class DestroyAfterNSeconds : MonoBehaviour {

	public float lifeTime;

	void OnEnable() {
		//Destroy(gameObject, lifeTime);
		Invoke("Destroy", lifeTime);
	}

	void Destroy() {
		gameObject.SetActive(false);
	}
}
