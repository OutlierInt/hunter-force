﻿using UnityEngine;
using System.Collections;

public class QuickScreenshot : MonoBehaviour {
	
	public KeyCode screenKey = KeyCode.ScrollLock;
	public int supersize = 2;

	private static float savedTime;
	private static int normalAA;
	private static int screenShotNumber = 0;
	void Update () {
		if(Input.GetKeyDown(screenKey))
			TakeScreenShot(supersize);
	}

	public static void TakeScreenShot(int supersize) {
		prepareForDoubleScreenshots(supersize);

		string captureFileName = /*System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + " " + */ Application.loadedLevelName + screenShotNumber;
		Application.CaptureScreenshot(captureFileName + ".png", supersize);

		finishDoubleScreenshots();
		screenShotNumber++;
		Debug.Log(captureFileName + " Taken");
	}

	public static void prepareForDoubleScreenshots(int superSizeAmount) {
		savedTime = Time.timeScale;
		normalAA = QualitySettings.antiAliasing;
		Time.timeScale = 0.001f;
		QualitySettings.antiAliasing = superSizeAmount <= 1 ? QualitySettings.antiAliasing : 0;
	}

	public static void finishDoubleScreenshots() {
		Time.timeScale = savedTime;
		QualitySettings.antiAliasing = normalAA;
	}

}
