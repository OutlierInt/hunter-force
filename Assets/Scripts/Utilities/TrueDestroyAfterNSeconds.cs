﻿using UnityEngine;
using System.Collections;

public class TrueDestroyAfterNSeconds : MonoBehaviour {

	public float lifeTime;

	void OnEnable() {
		Destroy(gameObject, lifeTime);
	}

}
