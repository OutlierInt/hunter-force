﻿using UnityEngine;
using System.Collections;

public static class Extensions {

	public static void ResetTransform (this Transform trans) {
		trans.localPosition = Vector3.zero;
		trans.localRotation = Quaternion.identity;
		trans.localScale = Vector3.one;
	}
}
