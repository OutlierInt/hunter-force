﻿using UnityEngine;
using System.Collections;

public class CruiseMissile2048 : MonoBehaviour {

	public Transform target;

	public float moveSpeed;
	public float minMoveTime, maxMoveTime;

	private bool moving = false;
	private Vector3 direction;
	private float moveTime;

	void Start() {
		target = FindObjectOfType<PlayerMovement>().transform;
	}

	void Update () {
		if(target != null) {
			if(!moving) {
				SelectDirection();
				moving = true;
			}
			else {
				transform.position += direction * moveSpeed * Time.deltaTime;
				moveTime -= Time.deltaTime;
				if(moveTime <= 0)
					moving = false;
			}
		}
	}

	void SelectDirection() {
		int selector = Random.Range(0,6);
		switch(selector) {
			case 0:
			case 1:
				direction = new Vector3(target.localPosition.x - transform.localPosition.x, 0, 0).normalized;
				break;

			case 2:
			case 3:
			case 4:
				direction = new Vector3( Mathf.Sign(target.localPosition.x - transform.localPosition.x), Mathf.Sign(target.localPosition.y - transform.localPosition.y), 0).normalized;
				break;

			case 5:
			case 6:
				direction = new Vector3(0, target.localPosition.y - transform.localPosition.y, 0).normalized;
				break;
		}
		moveTime = Random.Range(minMoveTime, maxMoveTime);
	}
}
