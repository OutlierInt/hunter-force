Please visit SCIFI 3D <www.theforce.net/scifi3d/> and SCIFI-MESHES.com
for rules governing use, display of credit, and copyright information.


� 2001 - Immense Class Starbase
-------------------------------

Created with: 3D Studio Max 3.1
Created by: Nir Schneider (Don't Shoot)

All pictures, movies, animations or anything else this model will
be shown at must have credits with my name (or nickname) on them.

Special thanks to Cyrille Lefevre (Tachy) for continual support and
tremendous help in building this model.

For any questions contact me at:
donshoot@netvision.net.il
Or go to my website at:
http://www22.brinkster.com/donshoot/

"May The Phasers Be With You..."