using UnityEngine;
using System.Collections;

namespace MyShmup
{
	public class CloudParticle : MonoBehaviour
	{
	
		
		// Use this for initialization
		void Start ()
		{
		// retrieve a reference to the Mesh of the object from the 
		// MeshFilter component
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		
		// get the vertex count for the objet
		int vCount = mesh.vertexCount ;
		
		// create an array of Colors with the same length
		Color[] new_vcolor = new Color[vCount];
	   // set the color for the corresponding vertex index in the array
	   // we choose a gray color with an alpha of 1
	   float gray = Random.Range (0f,0.15f);
	   new_vcolor[0] = new Color (gray,gray,gray,Random.Range (0.5f,0.6f)) ; 
	   new_vcolor[1] = new_vcolor[0]; 
	   new_vcolor[2] = new Color (1f,1f,1f,1f) ; 
	   new_vcolor[3] = new_vcolor[2]; 
		
		// replace the current vertex color array 
		//(white everywhere if it was untouched) 
		// with the newly created one
		mesh.colors = new_vcolor ;	

			
			//transform.localScale = new Vector3(Random.Range(2f,3f),Random.Range(2f,3f),1f);
			/*if( transform.position.y <= -25f ) {
				renderer.material.mainTextureOffset = new Vector2(0.75f,0.75f);
			
			} else*/ {
				int choice = Random.Range(0,100);
				switch( choice ) {
				/*case 0:
					renderer.material.mainTextureOffset = new Vector2(0f,0f);
					break;
				case 1:
					renderer.material.mainTextureOffset = new Vector2(0.25f,0f);
					break;
				case 2:
					renderer.material.mainTextureOffset = new Vector2(0.5f,0f);
					break;
				case 3:
					renderer.material.mainTextureOffset = new Vector2(0.5f,0.25f);
					break;
				case 4:
					renderer.material.mainTextureOffset = new Vector2(0.75f,0.25f);
					break;
				case 5:
					renderer.material.mainTextureOffset = new Vector2(0.75f,0f);
					break;*/
				default:
					if( choice < 2 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0f,0.25f);
					} else
					if( choice < 5 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0f,0.5f);
					} else
					if( choice < 7 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.25f,0.75f);
					} else
					if( choice < 9 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.25f,0.5f);
					} else
					if( choice < 12 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.5f,0.5f);
					} else
					if( choice < 14 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.5f,0.75f);
					} else
					if( choice < 16 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.75f,0.5f);
					} else
					if( choice < 57 ) {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0f,0.75f);
					} else {
						GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.25f,0.25f);
					}
					break;
				}
				
				
			}
		}
	}
}