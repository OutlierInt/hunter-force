using UnityEngine;
using System.Collections;

namespace MyShmup
{
	public class CloudManager : MonoBehaviour
	{
	
		public GameObject prefab;
		public int numberOfParticles;
		private CloudParticle[] particles;
	
		protected Vector3 speed;
		public bool front;
		// Use this for initialization
		void Start ()
		{
			int cloudsLayer = LayerMask.NameToLayer("Clouds");
			transform.position = new Vector3(Random.Range (-300f,700f), 6400f + Random.Range (-150f,150f),front ? Random.Range (-70f,-50f) : Random.Range (50f,300f));
			particles = new CloudParticle[numberOfParticles];
			//Random.seed = (int)Random.value * 123434;
			for (int i = 0; i < numberOfParticles; i++) {
				GameObject go = GameObject.Instantiate(prefab, transform.position + 
					new Vector3 (
					Random.Range (-8f*numberOfParticles, 8f*numberOfParticles), 
					Random.Range (-3f*numberOfParticles, 3f*numberOfParticles), 
					Random.Range (-numberOfParticles, 5f*numberOfParticles)), 
					Quaternion.Euler(0f,0f,Random.Range(-5f,5f))) as GameObject;
				go.layer = cloudsLayer;
				particles[i] = go.GetComponent<CloudParticle>();
				go.transform.parent = transform;
			}
			
			speed = new Vector3(Random.Range(60f,80f),Random.Range(-15f,-10f),0f);
		}
	
		// Update is called once per frame
		void Update ()
		{
			transform.position -= speed*Time.deltaTime;
			
			if(transform.position.x < /*Camera.main.transform.position.x*/ - 600f ) {
				transform.position = new Vector3(/*Camera.main.transform.position.x+*/Random.Range (500f,600f), 6400f + Random.Range (-150f,150f),front ? Random.Range (-70f,-50f) : Random.Range (50f,300f));
			}
		}
	}

}