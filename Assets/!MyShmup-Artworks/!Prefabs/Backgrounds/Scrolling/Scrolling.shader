// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "MyShmup/BackgroundScroller" {
	Properties {
		tex0 ("Base (RGB)", 2D) = "black" {}
		time ("Time", Float) = 0
		resolution ("Screen Resolution", Vector) = (640.0,480.0,0.0,0.0)
		horizon ("Horizon",Float) = 240.0
		tint ("Tint",Color) = (0.2,0.2,0.2,1.0)
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		LOD 200
		
		Pass {
		
      	Blend SrcAlpha OneMinusSrcAlpha
      	//Blend One One
      	Cull Back
      	ZWrite Off
      	Lighting Off
		
		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		sampler2D tex0;
		float time;
		float4 resolution;
		float horizon;
		float4 tint;

		struct v2f {
		  float4 position : POSITION;
		  float4 pos : TEXCOORD0;
		};

		v2f vert (float4 vertex : POSITION) {  
		    v2f OUT; 
			OUT.position = UnityObjectToClipPos( vertex);
			OUT.pos = vertex; //mul( UNITY_MATRIX_MVP, vertex);
			return OUT;
		}
		
		float4 frag (v2f IN) : COLOR {

		  float2 p = 2.0 * IN.pos.xy / resolution.xy;
		  float2 uv;
		  
    	  uv.x = -0.01 * time + 0.25 * p.x / p.y;
    	  uv.y = 0.5 / p.y;
    	  
		  // include light effect!
		  // include refraction
		  // include bump
		  // include orientation
		  
		  float4 fragColor = float4( tint.xyz * tex2D(tex0,uv).xyz * -p.y, 1.0 - step( horizon / (0.75 * resolution.y), p.y ) );
		  		  		  		  
		  return fragColor;	
		}


		ENDCG
		}
	} 
	FallBack "Diffuse"
}
